<?php

// Initialize
$init_path = '';
include($init_path.'lib/init.php');

$ajax_commands = array();

?><!doctype html>
<html class="no-js" lang="<?php echo $language; ?>">
<head>
	<base href="<?php echo $baseurl; ?>/" />
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>comboDB</title>
	<link rel="stylesheet" href="css/loading.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/motion-ui.min.css">
	<link rel="stylesheet" href="css/foundation-icons.css">

	<link rel="stylesheet" href="css/jquery-ui.min.css">
	<link rel="stylesheet" href="css/jquery-ui.structure.min.css">

	<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
	<link rel="manifest" href="site.webmanifest">
	<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<div class="loading" id="loading">Loading&#8230;</div>
	

	<!-- Sticky navigateion -->
	<div data-sticky-container>
		<div class="sticky" data-sticky data-sticky-on="small" data-margin-top="0">
			<!-- Mobile -->
			<div id="top-bar-mobile" class="grid-container show-for-small-only">
				<div class="grid-x grid-margin-x">
					<div class="cell small-3">
						<span class="logo-color-1">combo</span><span class="logo-color-2">DB</span>
					</div>
					<div class="cell small-9">
						<div class="button-group float-right no-bottom-margin">
							<a class="button" href="index.php?function=combos" title="<?php echo lng('combos'); ?>"><i class="fi-link"></i></a>
							<a class="button" href="index.php?function=moves" title="<?php echo lng('moves'); ?>"><i class="fi-list-thumbnails"></i></a>
							<a class="button" href="logout.php" title="<?php echo lng('log_out'); ?>"><i class="fi-lock"></i></a>
						</div>
					</div>
				</div>
			</div>

			<!-- Tablet and desktop -->
			<div id="top-bar-container" class="hide-for-small-only">
				<div class="grid-container">
					<div class="top-bar">
						<div class="top-bar-left">
							<ul class="menu" data-dropdown-menu>
								<li class="menu-text"><span class="logo-color-1">combo</span><span class="logo-color-2">DB</span></li>
							</ul>
						</div>
						<div class="top-bar-right">
							<ul class="menu">
								<li><a href="index.php?function=combos"><?php echo lng('combos'); ?></a></li>
								<li><a href="index.php?function=moves"><?php echo lng('moves'); ?></a></li>
								<li><a href="logout.php"><?php echo lng('log_out'); ?></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br />
	<?php

		$available_functions = array('combos', 'moves');

		if(!isset($_REQUEST['function'])) {
			$function = 'combos';
		} else {
			if(in_array($_REQUEST['function'], $available_functions)) {
				$function = $_REQUEST['function'];
			} else {
				$function = '404';
			}
		}
		include('lib/'.$function.'.php');

	?>

	<script src="js/functions.js"></script>
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/js/foundation.min.js" integrity="sha256-pRF3zifJRA9jXGv++b06qwtSqX1byFQOLjqa2PTEb2o=" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		
		$(document).ready(function() {
			$(document).foundation();

			$( ".sortable-moves" ).sortable({
				handle: ".sortable-handle",
				revert: true,
				update: function (event, ui) {
			        var data = $(this).sortable('serialize');
					// POST to server using $.post or $.ajax
					$.ajax({
						data: data,
						type: 'POST',
						url: 'lib/move_order.php',
						success : function(data) {
							console.log('Data: '+data);
						}
					});
				}
			});
			$( ".sortable-moves" ).disableSelection();

			$( ".sortable-combos" ).sortable({
				handle: ".sortable-handle",
				revert: true,
				update: function (event, ui) {
			        var data = $(this).sortable('serialize');
					// POST to server using $.post or $.ajax
					$.ajax({
						data: data,
						type: 'POST',
						url: 'lib/combo_order.php',
						success : function(data) {
							console.log('Data: '+data);
						}
					});
				}
			});
			$( ".sortable-combos" ).disableSelection();

		});

		$(window).on("load", function() {
			$( "#loading" ).fadeOut( "slow", function() {
			});			
		});

	</script>
</body>
</html>
