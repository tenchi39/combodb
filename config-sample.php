<?php

// ** Basic settings ** //
/** Subdirectory - If your installation is in a subdirectory, enter the directory name with a preceeding slash, eg.: "/phpsel" */
$subdirectory = '';

/** Protocol {http/https} */
$protocol = 'http';

/** Base url - You will probably not need to change this */
$baseurl = $protocol.'://'.$_SERVER['SERVER_NAME'].$subdirectory;


// ** MySQL settings - You can get this info from your web host ** //
/** MySQL hostname */
$db_host = '';

/** MySQL port (optional) - For MySQL the default port is 3306, for MariaDB it is 3307 */
$db_port = '';

/** The name of the database for W2W */
$db_database = '';

/** MySQL database username */
$db_user = '';

/** MySQL database password */
$db_password = '';


// ** Login settings - Be sure to change these! ** //
/** Login username */
$login_username = '';

/** Login password */
$login_password = '';

/** Guest username */
$guest_username = '';

/** Guest password */
$guest_password = '';


// ** Other settings ** //
/** Keep it on false if you are not actively debugging or modifying the code */
$development_mode = false;

/** Language */
$language = 'en';