<?php

	header('Content-type: text/html; charset=utf-8'); 
	// configuration
	include('config.php');

	// session
	session_name($db_database);
	session_start();

	// functions
	include('lib/functions.php');

	// language
	include('lng/'.$language.'.php');

	if(!isset($_REQUEST['username'])) {
		$_REQUEST['username'] = '';
	}

	if(!isset($_REQUEST['password'])) {
		$_REQUEST['password'] = '';
	}

	if(isset($_REQUEST['submitted'])) {
		if($_REQUEST['username'] == $login_username and $_REQUEST['password'] == $login_password) {
			$_SESSION['logged_in'] = true;
			$_SESSION['guest_session'] = false;
		}
		if($_REQUEST['username'] == $guest_username and $_REQUEST['password'] == $guest_password) {
			$_SESSION['logged_in'] = true;
			$_SESSION['guest_session'] = true;
		}
	}


	if(isset($_SESSION['logged_in'])) {
		header("Location: ".$baseurl."/index.php");
	}

?>


<!doctype html>
<html class="no-js" lang="<?php echo $language; ?>">
<head>
	<base href="<?php echo $baseurl; ?>/" />
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>comboDB</title>
	<link rel="stylesheet" href="css/loading.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/motion-ui.min.css">
	<link rel="stylesheet" href="css/foundation-icons.css">

	<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
	<link rel="manifest" href="site.webmanifest">
	<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<div class="loading" id="loading">Loading&#8230;</div>

	<div id="main">

		<br />
		<br />
		<br />

		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-10 small-offset-1 medium-6 large-4 medium-offset-3 large-offset-4">
					<fieldset class="fieldset">
  						<legend><?php echo lng('log_in_to_combodb'); ?></legend>
						<form class="log-in-form" action="<?php echo $baseurl; ?>/login.php" method="POST">
							<input type="hidden" name="submitted" value="true">
							<!-- <h4 class="text-center"><?php echo lng('log_in_to_phpsel'); ?></h4> -->
							<br />
							<?php
								if(isset($_REQUEST['submitted'])) {
									if($_REQUEST['username'] != $login_username or $_REQUEST['password'] != $login_password) {
										echo '<div class="callout alert">';
											echo '<h5>'.lng('error').'</h5>';
											echo '<p>'.lng('incorrect_username_or_password').'</p>';
										echo '</div>';
									}
								}
							?>
							<label><?php echo lng('username'); ?>
								<input type="text" name="username" value="<?php echo $_REQUEST['username']; ?>">
							</label>
							<label><?php echo lng('password'); ?>
								<input type="password" name="password" value="<?php echo $_REQUEST['password']; ?>">
							</label>
							<br />
							<p><input type="submit" class="button expanded" value="<?php echo lng('log_in'); ?>"></input></p>
						</form>
					</fieldset>
				</div>
			</div>
		</div>
	</div>




	<script src="js/functions.js"></script>
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/js/foundation.min.js" integrity="sha256-pRF3zifJRA9jXGv++b06qwtSqX1byFQOLjqa2PTEb2o=" crossorigin="anonymous"></script>
	<script>

		$(document).ready(function() {
			$(document).foundation();
		});

		$(window).on("load", function() {
			$( "#loading" ).fadeOut( "slow", function() {
			});			
		});

	</script>

</body>
</html>