<?php

function countMysqlItems($table, $where) {
	global $link;
	$result = mysqli_query($link, "SELECT ".$table."_id FROM ".$table." ".$where);
    $counter = mysqli_num_rows($result);
	return $counter;
}

function lng($id) {
	global $lng;
	return $lng[$id];
}