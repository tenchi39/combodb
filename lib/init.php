<?php

header('Content-type: text/html; charset=utf-8'); 


// Strip tags for security
foreach($_REQUEST as $key => $value) {
	//$_REQUEST[$key] = strip_tags($value);
}

// Import configuration
include($init_path.'config.php');

session_name($db_database);
session_start();

if($development_mode == true) {
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

// Import necessary functions
include($init_path.'lib/functions.php');

// Import language
include($init_path.'lng/'.$language.'.php');

// authorization
if(!isset($_SESSION['logged_in'])) {
	header("Location: ".$baseurl."/login.php");
	exit();
}

// Open MySQL connection
include($init_path.'lib/database.php');