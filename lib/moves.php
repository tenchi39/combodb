<?php

	$show_move_form = false;
	$show_combo_form = false;
	$error_move_exists = false;
	$error_combo_exists = false;
	$error_number_of_moves_in_combo = false;
	$success_move = false;
	$success_combo = false;

	// Add move
	if(isset($_REQUEST['action']) and $_REQUEST['action'] == 'add_move') {
		$show_move_form = true;
		// We only add this move if it doesn't exist already
		if(countMysqlItems('moves', "WHERE moves_name='".mysqli_real_escape_string($link, $_REQUEST['moves_name'])."' AND moves_type='".mysqli_real_escape_string($link, $_REQUEST['moves_type'])."'") != 0) {
			$error_move_exists = true;
		} else {
			$counter = countMysqlItems('moves', "WHERE moves_type='".mysqli_real_escape_string($link, $_REQUEST['moves_type'])."'");
			$counter++;
			mysqli_query($link, "INSERT INTO moves (moves_name, moves_type, moves_order) VALUES (
				'".mysqli_real_escape_string($link, $_REQUEST['moves_name'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['moves_type'])."', 
				'".$counter."' 
			)");
			echo mysqli_error($link);
			unset($_REQUEST['moves_name']);
			unset($_REQUEST['moves_type']);
			$success_move = true;
		}
	}

	// Delete move
	if(isset($_REQUEST['action']) and $_REQUEST['action'] == 'delete_move') {
		mysqli_query($link, "DELETE FROM moves WHERE moves_id='".mysqli_real_escape_string($link, $_REQUEST['move_id'])."' LIMIT 1");
		echo mysqli_error($link);
		// Reorder other moves of the same type
		$i = 1;
		$result = mysqli_query($link, "SELECT * FROM moves WHERE moves_type='".mysqli_real_escape_string($link, $_REQUEST['move_type'])."' ORDER BY moves_order");
		while($myrow = mysqli_fetch_assoc($result)) {
			mysqli_query($link, "UPDATE moves SET moves_order='".$i."' WHERE moves_id='".$myrow['moves_id']."' LIMIT 1");
			echo mysqli_error($link);
			$i++;
		}
	}

	// Add combo
	if(isset($_REQUEST['action']) and $_REQUEST['action'] == 'add_combo') {
		$show_combo_form = true;
		// Number of moves in the combo
		$moves_array = explode(",", $_REQUEST['combo_moves']);
		$number = count($moves_array);
		if($number < 2) {
			$error_number_of_moves_in_combo = true;
		} else {
			// We only add this move if it doesn't exist already
			if(countMysqlItems('combos', "WHERE combos_moves='".mysqli_real_escape_string($link, $_REQUEST['combo_moves'])."'") != 0) {
				$error_combo_exists = true;
			} else {
				$counter = countMysqlItems('combos', "WHERE combos_number='".$number."'");
				$counter++;
				mysqli_query($link, "INSERT INTO combos (combos_name, combos_number, combos_moves, combos_order) VALUES (
					'".mysqli_real_escape_string($link, $_REQUEST['combo_name'])."', 
					'".$number."', 
					'".mysqli_real_escape_string($link, $_REQUEST['combo_moves'])."', 
					'".$counter."' 
				)");
				echo mysqli_error($link);
				unset($_REQUEST['combo_name']);
				unset($_REQUEST['combo_moves']);
				$success_combo = true;
			}

		}
	}




?>

<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12">
			<h1><?php echo lng('moves'); ?></h1>

			<?php if($_SESSION['guest_session'] == false): ?>
			
				<a class="" onclick="removeClass('add_move_form', 'hide');"><i class="fi-plus"></i> <?php echo lng('add_move'); ?></a><br>
				<a class="" onclick="removeClass('combo_form', 'hide');"><i class="fi-plus"></i> <?php echo lng('add_combo'); ?></a><br>
				<br>
			
			<?php endif ?>
		</div>

		<?php if($_SESSION['guest_session'] == false): ?>
		<!-- Move form -->
		<div class="cell small-12<?php if($show_move_form == false) {echo ' hide';}; ?>" id="add_move_form">
			<fieldset class="fieldset">
	  		<legend><?php echo lng('add_move'); ?></legend>
			<form action="index.php?function=moves" method="POST">
				<input type="hidden" name="action" value="add_move"></input>
				<?php
					if($error_move_exists == true) {
						echo '<span class="red">'.lng('error_move_exists').'</span>';
					}
					if($success_move == true) {
						echo '<span class="green">'.lng('move_added_successfully').'</span>';
					}
				?>
				<label><?php echo lng('name'); ?>:
					<input type="text" name="moves_name" required<?php if(isset($_REQUEST['moves_name'])) {echo ' value="'.$_REQUEST['moves_name'].'"';} ?>>
				</label>

				<label>Type:<br />
				<input type="radio" name="moves_type" value="1" id="value_punch" required<?php if(isset($_REQUEST['moves_type']) and $_REQUEST['moves_type'] == 1) {echo ' checked';} ?>><label for="value_punch"><?php echo lng('punch'); ?></label><br />
				<input type="radio" name="moves_type" value="2" id="value_kick"<?php if(isset($_REQUEST['moves_type']) and $_REQUEST['moves_type'] == 2) {echo ' checked';} ?>><label for="value_kick"><?php echo lng('kick'); ?></label><br />
				<input type="radio" name="moves_type" value="3" id="value_defense"<?php if(isset($_REQUEST['moves_type']) and $_REQUEST['moves_type'] == 3) {echo ' checked';} ?>><label for="value_defense"><?php echo lng('defense'); ?></label><br />
				<br>
				<input type="submit" class="button" value="<?php echo lng('submit'); ?>">
				<span class="float-right">[<a onclick="addClass('add_move_form', 'hide');">Close</a>]</span>
				</label>
			</form>
			</fieldset>
		</div>
		
		<!-- Combo form -->
		<div class="cell small-12<?php if($show_combo_form == false) {echo ' hide';}; ?>" id="combo_form">
			<fieldset class="fieldset">
	  		<legend><?php echo lng('add_combo'); ?></legend>
			<form action="index.php?function=moves" method="POST">
				<input type="hidden" name="action" value="add_combo"></input>
				<?php
					if($error_combo_exists == true) {
						echo '<span class="red">'.lng('error_combo_exists').'</span>';
					}
					if($error_number_of_moves_in_combo == true) {
						echo '<span class="red">'.lng('error_combo_number').'</span>';
					}
					if($success_combo == true) {
						echo '<span class="green">'.lng('combo_added_successfully').'</span>';
					}
				?>
				<label><?php echo lng('name'); ?>:
					<input type="text" name="combo_name" placeholder="<?php echo lng('adding_a_name_is_not_compulsory'); ?>"<?php if(isset($_REQUEST['combo_name'])) {echo ' value="'.$_REQUEST['combo_name'].'"';} ?>>
				</label>
				<label><?php echo lng('moves'); ?>:
					<input type="text" readonly name="combo_moves" id="combo_moves" required placeholder="<?php echo lng('add_moves_by'); ?>"<?php if(isset($_REQUEST['combo_moves'])) {echo ' value="'.$_REQUEST['combo_moves'].'"';} ?>>
				</label>

				<br>
				<input type="submit" class="button" value="<?php echo lng('submit'); ?>">
				<span class="float-right">[<a onclick="addClass('combo_form', 'hide');">Close</a>]</span>
				</label>
			</form>
			</fieldset>
		</div>
		<?php endif ?>
		
		<div class="cell small-12 medium-4 sortable-moves">
			<h4><?php echo lng('punches'); ?></h4>
			<?php
				$result = mysqli_query($link, "SELECT * FROM moves WHERE moves_type='1' ORDER BY moves_order");
				while($myrow = mysqli_fetch_assoc($result)) {
					echo '<div class="sortable-items" id="item-'.$myrow['moves_id'].'">';
					if($_SESSION['guest_session'] == false) {
						echo '<a class="sortable-handle faint" title="'.lng('move').'"><i class="fi-list"></i></a>&nbsp;';
						echo '<a class="faint" href="index.php?function=moves&amp;action=delete_move&amp;move_id='.$myrow['moves_id'].'&amp;move_type='.$myrow['moves_type'].'" onclick="return confirm(\''.lng('are_you_sure').'\')" title="'.lng('delete').'"><i class="fi-trash"></i></a>&nbsp;&nbsp;&nbsp;';
						echo '<a title="'.lng('add_to_combo').'" onclick="addToCombo(\''.$myrow['moves_id'].'\');"><i class="fi-plus"></i></a>&nbsp;';
					} else {
						echo '#'.$myrow['moves_order'].': ';
					}
					echo $myrow['moves_name'].'<br>';
					echo '</div>';
				}
			?>
		<br>
		</div>
		<div class="cell small-12 medium-4 sortable-moves">
			<h4><?php echo lng('kicks'); ?></h4>
			<?php
				$result = mysqli_query($link, "SELECT * FROM moves WHERE moves_type='2' ORDER BY moves_order");
				while($myrow = mysqli_fetch_assoc($result)) {
					echo '<div class="sortable-items" id="item-'.$myrow['moves_id'].'">';
					if($_SESSION['guest_session'] == false) {
						echo '<a class="sortable-handle faint" title="'.lng('move').'"><i class="fi-list"></i></a>&nbsp;';
						echo '<a class="faint" href="index.php?function=moves&amp;action=delete_move&amp;move_id='.$myrow['moves_id'].'&amp;move_type='.$myrow['moves_type'].'" onclick="return confirm(\''.lng('are_you_sure').'\')" title="'.lng('delete').'"><i class="fi-trash"></i></a>&nbsp;&nbsp;&nbsp;';
						echo '<a title="'.lng('add_to_combo').'" onclick="addToCombo(\''.$myrow['moves_id'].'\');"><i class="fi-plus"></i></a>&nbsp;';
					} else {
						echo '#'.$myrow['moves_order'].': ';
					}
					echo $myrow['moves_name'].'<br>';
					echo '</div>';
				}
			?>
		<br>
		</div>
		<div class="cell small-12 medium-4 sortable-moves">
			<h4><?php echo lng('defense'); ?></h4>
			<?php
				$result = mysqli_query($link, "SELECT * FROM moves WHERE moves_type='3' ORDER BY moves_order");
				while($myrow = mysqli_fetch_assoc($result)) {
					echo '<div class="sortable-items" id="item-'.$myrow['moves_id'].'">';
					if($_SESSION['guest_session'] == false) {
						echo '<a class="sortable-handle faint" title="'.lng('move').'"><i class="fi-list"></i></a>&nbsp;';
						echo '<a class="faint" href="index.php?function=moves&amp;action=delete_move&amp;move_id='.$myrow['moves_id'].'&amp;move_type='.$myrow['moves_type'].'" onclick="return confirm(\''.lng('are_you_sure').'\')" title="'.lng('delete').'"><i class="fi-trash"></i></a>&nbsp;&nbsp;&nbsp;';
						echo '<a title="'.lng('add_to_combo').'" onclick="addToCombo(\''.$myrow['moves_id'].'\');"><i class="fi-plus"></i></a>&nbsp;';
					} else {
						echo '#'.$myrow['moves_order'].': ';
					}
					echo $myrow['moves_name'].'<br>';
					echo '</div>';
				}
			?>
		<br>
		</div>
	</div>
</div>