<?php


	// Delete combo
	if(isset($_REQUEST['action']) and $_REQUEST['action'] == 'delete_combo') {
		mysqli_query($link, "DELETE FROM combos WHERE combos_id='".mysqli_real_escape_string($link, $_REQUEST['combo_id'])."' LIMIT 1");
		echo mysqli_error($link);
		// Reorder other moves of the same type
		$i = 1;
		$result = mysqli_query($link, "SELECT * FROM combos WHERE combos_number='".mysqli_real_escape_string($link, $_REQUEST['combo_number'])."' ORDER BY combos_order");
		while($myrow = mysqli_fetch_assoc($result)) {
			mysqli_query($link, "UPDATE combos SET combos_order='".$i."' WHERE combos_id='".$myrow['combos_id']."' LIMIT 1");
			echo mysqli_error($link);
			$i++;
		}
	}


?>


<div class="grid-container">

<div class="grid-x grid-margin-x">
	<div class="cell small-12">
		<h1><?php echo lng('combos'); ?></h1>
	</div>
	<div class="cell small-12">

		<?php

			// Get all moves
			$result_moves = mysqli_query($link, "SELECT * FROM moves");
			while($myrow_moves = mysqli_fetch_assoc($result_moves)) {
				$move_names[$myrow_moves['moves_id']] = $myrow_moves['moves_name'];
				$move_types[$myrow_moves['moves_id']] = $myrow_moves['moves_type'];
			}

			// Get the maximum number of moves
			$result_maxmoves = mysqli_query($link, "SELECT combos_number FROM combos ORDER BY combos_number DESC LIMIT 1");
			$myrow_maxmoves = mysqli_fetch_assoc($result_maxmoves);

			// List combos in grouped by the number of moves
			for($i = 2; $i <= $myrow_maxmoves['combos_number']; $i++) {
				echo '<br>';
				echo '<h4>'.str_replace('XXX', $i, lng('combos_with_x_moves')).'</h4>';
				echo '<div class="cell small-12 sortable-combos">';
				$result = mysqli_query($link, "SELECT * FROM combos WHERE combos_number='".$i."' ORDER BY combos_order");
				while($myrow = mysqli_fetch_assoc($result)) {
					echo '<div class="sortable-items" id="item-'.$myrow['combos_id'].'">';
					echo '<fieldset class="fieldset smallfieldset">';
			  		echo '<legend>';
					if($_SESSION['guest_session'] == false) {
						echo '<a class="sortable-handle faint" title="'.lng('move').'"><i class="fi-list"></i></a>&nbsp;';
						echo '<a class="faint" href="index.php?function=combos&amp;action=delete_combo&amp;combo_id='.$myrow['combos_id'].'&amp;combo_number='.$myrow['combos_number'].'" onclick="return confirm(\''.lng('are_you_sure').'\')" title="'.lng('delete').'"><i class="fi-trash"></i></a>&nbsp;&nbsp;&nbsp;';
					}
			  		echo '#'.$myrow['combos_order'].': ';
					if($myrow['combos_name'] == '') {
						echo lng('noname_combo');
					} else {
						echo $myrow['combos_name'];
					}
			  		echo '</legend>';
					$first = true;
					$move_array = explode(',', $myrow['combos_moves']);
					foreach ($move_array as $key => $value) {
						if($first == true) {
							$first = false;
						} else {
							echo ' + ';
						}
						if(!isset($move_names[$value])) {
							echo '<span class="label warning">'.lng('error_deleted_move').'</span>';
						} else {
							echo '<span class="label ';
							switch ($move_types[$value]) {
							case 1:
							    echo "primary";
							    break;
							case 2:
							    echo "success";
							    break;
							case 3:
							    echo "alert";
							    break;
							}
							echo '">';
							echo $move_names[$value];
							echo '</span>';
						}
					}
					echo '</fieldset>';
					echo '</div>';
				}

				echo '</div>';

			}


		?>


	</div>
</div>

</div>