function removeClass(id, name) {
	$('#' + id).removeClass(name);
}

function addClass(id, name) {
	$('#' + id).addClass(name);
}

function addValue(id, value) {
	var content = $('#' + id).val();
	if(content == '') {
		content += value;	
	} else {
		content += ',' + value;
	}
	$('#' + id).val(content);
}

function addToCombo(id) {
	removeClass('combo_form', 'hide');
	addValue('combo_moves', id);
}